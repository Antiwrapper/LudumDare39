extends Control

func _ready():
	set_process(true);

func _process(delta):
	if (get_parent().get_node("Player").has_node("Camera")):
		set_pos(get_parent().get_node("Player/Camera").get_camera_screen_center() - Vector2(512, 300));	