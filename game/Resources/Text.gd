extends RichTextLabel

var Op = 0;
var Played = false;

func _ready():
	set_fixed_process(true);

func _fixed_process(delta):
	if (get_parent().get_node("Player").Energy > 10):
		Op += delta / 3;
		set_opacity(Op);
	if (Op > 2 && Played == false):
		get_node("/root/Global").MusicPlayer.stop();
		get_node("/root/Global").SoundPlayer.play("Hit");
		Played = true;
		get_parent().get_node("Player").Disabled = true;
		get_parent().get_node("Player").set_hidden(true);
		
	if (Op > 3):
		get_node("/root/Global").Level10 = true;
		get_node("/root/Global").MusicPlayer.play();
		get_node("/root/Global").goto_scene("res://Scenes/MainMenu.tscn");