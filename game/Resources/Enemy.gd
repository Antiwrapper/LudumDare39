extends KinematicBody2D

const SPEED=180;

var StartPos = Vector2();
var RotSpeed = 0;
var AccelRot = 10;
var Rot = 0;

func _ready():
	StartPos = get_pos();
	add_to_group("Enemies");
	set_fixed_process(true);

func _fixed_process(delta):
	var space_state = get_world_2d().get_direct_space_state();
	var result = space_state.intersect_ray(get_pos(), get_parent().get_node("Player").get_pos(), [self]);
	var dir = Vector2(); 
	if (result.empty() == false && result.collider.get_name() == "Player"):
		dir = get_parent().get_node("Player").get_pos() - get_pos();
		if (RotSpeed < 60):
			RotSpeed += AccelRot * delta;
	elif (RotSpeed > 0):
		RotSpeed -= AccelRot * delta;
	dir = dir.normalized();
	Rot += RotSpeed;
	if (Rot > 360):
		Rot -= 360;
	get_node("Sprite").set_rot(deg2rad(Rot));
	var motion = move(dir * SPEED * delta);
	if (is_colliding()):
		var player = get_parent().get_node("Player");
		if (get_collider() == player && get_parent().get_node("Player/AP").is_playing() == false):
			if (player.Dead != true):
				get_node("/root/Global").SoundPlayer.play("Hit");
			player.Dead = true;
			player.get_node("AP").play("Death");
			player.Disabled = true;
		var n = get_collision_normal();
		motion = n.slide(motion);
		move(motion);

func reset():
	set_pos(StartPos);
	RotSpeed = 0;
	Rot = 0;