extends GridContainer

func _ready():
	if (get_node("/root/Global").Level1 == true):
		get_node("Button").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level2 == true):
		get_node("Button1").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level3 == true):
		get_node("Button2").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level4 == true):
		get_node("Button3").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level5 == true):
		get_node("Button4").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level6 == true):
		get_node("Button5").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level7 == true):
		get_node("Button6").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level8 == true):
		get_node("Button7").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level9 == true):
		get_node("Button8").set_button_icon(preload("res://Art/Finish.png"));
	if (get_node("/root/Global").Level10 == true):
		get_node("Button9").set_button_icon(preload("res://Art/Finish.png"));

func _on_Button_pressed():
	get_node("/root/Global").Level = 1;
	get_node("/root/Global").goto_scene("res://Scenes/Level1.tscn");

func _on_Button1_pressed():
	get_node("/root/Global").Level = 2;
	get_node("/root/Global").goto_scene("res://Scenes/Level2.tscn");

func _on_Button2_pressed():
	get_node("/root/Global").Level = 3;
	get_node("/root/Global").goto_scene("res://Scenes/Level3.tscn");

func _on_Button3_pressed():
	get_node("/root/Global").Level = 4;
	get_node("/root/Global").goto_scene("res://Scenes/Level4.tscn");

func _on_Button4_pressed():
	get_node("/root/Global").Level = 5;
	get_node("/root/Global").goto_scene("res://Scenes/Level5.tscn");

func _on_Button5_pressed():
	get_node("/root/Global").Level = 6;
	get_node("/root/Global").goto_scene("res://Scenes/Level6.tscn");

func _on_Button6_pressed():
	get_node("/root/Global").Level = 7;
	get_node("/root/Global").goto_scene("res://Scenes/Level7.tscn");

func _on_Button7_pressed():
	get_node("/root/Global").Level = 8;
	get_node("/root/Global").goto_scene("res://Scenes/Level8.tscn");

func _on_Button8_pressed():
	get_node("/root/Global").Level = 9;
	get_node("/root/Global").goto_scene("res://Scenes/Level9.tscn");

func _on_Button9_pressed():
	get_node("/root/Global").Level = 10;
	get_node("/root/Global").goto_scene("res://Scenes/Level10.tscn");
