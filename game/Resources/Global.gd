extends Node

var CurrentScene = null;
var Level = 1;
var MusicPlayer = null;
var SoundPlayer = null;
var Level1 = false;
var Level2 = false;
var Level3 = false;
var Level4 = false;
var Level5 = false;
var Level6 = false;
var Level7 = false;
var Level8 = false;
var Level9 = false;
var Level10 = false;

func _ready():
	MusicPlayer = StreamPlayer.new();
	var audiostream = preload("res://Art/Soundtrack.ogg");
	MusicPlayer.set_stream(audiostream);
	MusicPlayer.set_volume(1.0);
	MusicPlayer.set_loop(true);
	add_child(MusicPlayer);
	
	SoundPlayer = SamplePlayer.new();
	var samples = load("res://Resources/Samples.tres");
	SoundPlayer.set_sample_library(samples);
	add_child(SoundPlayer);
	set_fixed_process(true);
	var root = get_tree().get_root();
	CurrentScene = root.get_child(root.get_child_count() - 1);
	MusicPlayer.play();

func _fixed_process(delta):
	if (Input.is_action_pressed("ui_cancel")):
		goto_scene("res://Scenes/MainMenu.tscn");

func goto_scene(path):
	call_deferred("_deferred_goto_scene", path);

func _deferred_goto_scene(path):
    CurrentScene.free()
    var s = ResourceLoader.load(path)
    CurrentScene = s.instance()
    get_tree().get_root().add_child(CurrentScene)
    get_tree().set_current_scene(CurrentScene)

func NextLevel():
	if (Level == 1):
		Level1 = true;
	elif (Level == 2):
		Level2 = true;
	elif (Level == 3):
		Level3 = true;
	elif (Level == 4):
		Level4 = true;
	elif (Level == 5):
		Level5 = true;
	elif (Level == 6):
		Level6 = true;
	elif (Level == 7):
		Level7 = true;
	elif (Level == 8):
		Level8 = true;
	elif (Level == 9):
		Level9 = true;
	elif (Level == 10):
		Level10 = true;
	if (Level < 10):
		Level += 1;
		goto_scene("res://Scenes/Level" + var2str(Level) + ".tscn");

