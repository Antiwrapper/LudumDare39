extends KinematicBody2D

const MAXSPEED = 250;

var StartPos = Vector2();
var RotSpeed = 0;
var AccelRot = 15;
var Rot = 0;
var Dir = Vector2();
var Activate = false;
var Time = 3;
var Disabled = false;

func _ready():
	StartPos = get_pos();
	add_to_group("Enemies");
	set_fixed_process(true);

func _fixed_process(delta):
	if (Disabled && Time > 0):
		Time -= delta;
	elif (Disabled):
		get_node("Explosion").set_emitting(false);
	else:
		if (Activate == false):
			var space_state = get_world_2d().get_direct_space_state();
			var result = space_state.intersect_ray(get_pos(), get_parent().get_node("Player").get_pos(), [self]);
			if (result.empty() == false && result.collider.get_name() == "Player"):
				Dir = get_parent().get_node("Player").get_pos() - get_pos();
				if (Dir.length() < 300):
					Activate = true;
				Dir = Dir.normalized();
		elif (RotSpeed < 40):
			RotSpeed += AccelRot * delta;
		else:
			Dir = get_parent().get_node("Player").get_pos() - get_pos();
			Dir = Dir.normalized();
			var motion = move(Dir * MAXSPEED * delta);
			if (is_colliding()):
				var n = get_collision_normal();
				motion = n.slide(motion);
				move(motion);
			Time -= delta;
			if (Time <= 0):
				explode();
		Rot += RotSpeed;
		if (Rot > 360):
			Rot -= 360;
		get_node("Sprite").set_rot(deg2rad(Rot));

func reset():
	get_node("Explosion").set_emitting(false);
	get_node("Sprite").set_hidden(false);
	set_collision_mask_bit(0, true);
	set_layer_mask_bit(0, true);
	set_pos(StartPos);
	Disabled = false;
	Activate = false;
	RotSpeed = 0;
	Rot = 0;
	Time = 3;

func explode():
	get_node("Explosion").set_emitting(true);
	get_node("/root/Global").SoundPlayer.play("Explosion", true);
	if ((get_parent().get_node("Player").get_pos() - get_pos()).length() < 64 && get_parent().get_node("Player/AP").is_playing() == false):
		var player = get_parent().get_node("Player");
		if (player.Dead != true):
			get_node("/root/Global").SoundPlayer.play("Hit");
		player.Dead = true;
		player.get_node("AP").play("Death");
		player.Disabled = true;
	get_node("Sprite").set_hidden(true);
	Time = 0.5;
	set_collision_mask_bit(0, false);
	set_layer_mask_bit(0, false);
	Disabled = true;
	Activate = false;
	RotSpeed = 0;
	Rot = 0;
	Time = 3;