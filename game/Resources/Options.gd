extends CheckButton

func _on_CheckButton_toggled( pressed ):
	if (pressed == true):
		OS.set_window_fullscreen(true);
	else:
		OS.set_window_fullscreen(false);

func _on_HSlider1_value_changed( value ):
	get_node("/root/Global").SoundPlayer.set_default_volume(value / 100);

func _on_HSlider_value_changed( value ):
	get_node("/root/Global").MusicPlayer.set_volume(value / 100);
