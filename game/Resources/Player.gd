extends KinematicBody2D

const SPEED=300;
const LONGJUMP=400;
const GRAVITY=880;
const MAXENERGY = 10;

var GravityAccel = GRAVITY;
var Gravity = 0;
var LongJump = false;
var GravityDir = Vector2(0, 1);
var Energy = 0;
var Ground = false;
var Disabled = false;
var StartPos = Vector2();
var Dead = false;
var Frames = 0;

func _ready():
	StartPos = get_pos();
	set_fixed_process(true);

func _fixed_process(delta):
	if (Input.is_action_pressed("restart")):
		LongJump = false;
		Gravity = 0;
		for e in get_tree().get_nodes_in_group("Enemies"):
			e.reset();
		if (get_node("AP").is_playing()):
			get_node("AP").seek(0, true);
			get_node("AP").stop();
		get_node("Sprite").set_scale(Vector2(1.0, 1.0));
		get_node("Sprite").set_hidden(false);
		Dead = false;
		Energy = 0;
		Disabled = false;
		set_pos(StartPos);
	if (Disabled == false):
		Gravity += GravityAccel * delta;
		Frames += 0.2;
		get_parent().get_node("Light").set_energy(Energy/MAXENERGY);
		get_parent().get_node("GUI/ProgressBar").set_value(100 - (Energy/MAXENERGY * 100));
		
		var dir = Vector2();
		
		if (Input.is_action_pressed("right")):
			get_node("Sprite").set_flip_h(false);
			dir.x += 1;
			Energy += delta;
		if (Input.is_action_pressed("left")):
			get_node("Sprite").set_flip_h(true);
			dir.x -= 1;
			Energy += delta;
		if (Input.is_action_pressed("jump") && Ground == true):
			get_node("Sprite").set_frame(1);
			if (!GravityAccel < GRAVITY):
				get_node("/root/Global").SoundPlayer.play("Jump");
				Energy += 0.5;
			if (Input.is_action_pressed("dash")):
				LongJump = true;
				if (!GravityAccel < GRAVITY):
					Energy += 0.2;
				Gravity -= 275;
			else:
				Gravity -= 375;
			Ground = false;
			GravityAccel = GRAVITY * 0.6;
		elif (!Input.is_action_pressed("jump") || Gravity > 0):
			GravityAccel = GRAVITY;
		dir = dir.normalized();

		
		if (Frames > 7):
			Frames = 0;
		if (Ground == true && dir.x != 0):
			get_node("Sprite").set_frame(round(Frames) + 2);
		elif (dir.x == 0 && Ground == true):
			LongJump = false;
			Frames = 0;
			get_node("Sprite").set_frame(0);
		
		var motion = move(dir * SPEED * delta + (Gravity * GravityDir * delta) + Vector2(dir.x * LONGJUMP * LongJump * delta, 0));
		
		if (is_colliding()):
			if (get_collider().get_name() == "FinishBody"):
				Disabled = true;
				get_node("AP").play("Win");
				get_node("/root/Global").SoundPlayer.play("Win");
			elif (get_collider().get_name().begins_with("SpikeBody") || get_collider().get_name().begins_with("Enemy")):
				if (Dead != true):
					get_node("/root/Global").SoundPlayer.play("Hit");
				Dead = true;
				get_node("AP").play("Death");
				Disabled = true;
			var n = get_collision_normal();
			if (n.y > 0.4):
				Gravity = 0;
			if (n.y < -0.4):
				Gravity = 0;
				Ground = true;
				LongJump = false;
			motion = n.slide(motion);
			move(motion);
	else:
		if (get_node("AP").is_playing() == false && Dead == false && get_node("/root/Global").Level != 10):
			get_node("/root/Global").NextLevel();
		elif (get_node("AP").is_playing() == false && get_node("/root/Global").Level != 10):
			for e in get_tree().get_nodes_in_group("Enemies"):
				e.reset();
			get_node("AP").seek(0, true);
			set_scale(Vector2(1, 1));
			Dead = false;
			Energy = 0;
			Disabled = false;
			set_pos(StartPos);